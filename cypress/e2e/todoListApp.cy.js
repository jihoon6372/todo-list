const EMPTY_TEXT = "There are no to-do items. Please write your to-dos.";

const todoListItems = ["신규 입사자 교육", "자바스크립트 공부"];

describe("Todo List App", () => {
  before(() => {
    cy.visit("/");
  });

  it("test", () => {
    cy.get(".wrapper").should("exist");
  });

  it(`할 일 항목이 존재하지 않는다면 "${EMPTY_TEXT}" 메시지를 노출한다.`, () => {
    cy.get(".todo-list > ul")
      .should("not.exist")
      .then(() => {
        cy.get(".todo-list").contains(EMPTY_TEXT);
      });
  });

  describe("할 일을 입력한 후 엔터키를 누르면", () => {
    it("입력한 항목이 할 일 목록에 노출된다.", () => {
      const input = cy.get("input");

      // 항목 추가
      todoListItems.forEach((item) => {
        input.type(item).type("{enter}");

        // 항목 체크
        cy.get("ul > li").contains(item);
      });
    });

    it('할 일 항목의 완료하지 않은 개수가 "N items left"로 노출된다.', () => {
      cy.get("ul")
        .find("li > input")
        .should("not.be.checked")
        .then((el) => {
          cy.get(".footer-left").contains(`${el.length} items left`);
        });
    });
  });

  describe("할 일 항목을 체크하면", () => {
    it("완료된 항목은 목록의 맨 하단으로 이동된다.", () => {
      cy.get("ul > li").first().find("input").check();
    });

    //   it("완료된 항목을 체크 해제하여 완료되지 않은 항목으로 바꾸면 목록의 맨 상단으로 이동된다.", () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });

    //   it("완료하지 않은 항목 갯수가 갱신된다.", () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });

    //   it("완료된 항목 갯수가 갱신된다.", () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });
    // });

    // describe('"Clear completed" 버튼을 클릭하면', () => {
    //   it("완료된 항목은 목록에서 제거된다.", () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });

    //   it("완료된 항목 갯수가 0으로 초기화 된다.", () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });
    // });

    // describe("보기 타입 버튼 메뉴에서", () => {
    //   it('"Active" 버튼을 클릭하면 완료되지 않은 목록을 노출한다.', () => {
    //     // test code
    //     expect(false).to.eq(true);
    //   });

    //   it('"Completed" 버튼을 클릭하면 완료된 항목을 노출한다.', () => {
    //     // test code
    //     // expect(false).to.eq(true);
    //   });
  });
});
