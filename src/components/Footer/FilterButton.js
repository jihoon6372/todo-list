export const createFilterButton = (props) => {
  const { label, isActive, onClick } = props;

  // 버튼 엘리먼트 생성
  const btn = document.createElement("button");

  // 클래스 추가
  btn.classList.add("filter-btn");

  // 텍스트 설정
  btn.innerText = label;

  // 클릭 이벤트 추가
  btn.addEventListener("click", onClick);

  // 활성화 여부가 참일 경우 active 클래스 추가
  if (isActive) {
    btn.classList.add("active");
  }

  return btn;
};
