import { createFooter } from "./Footer";

export default {
  title: "Status Section",
  argTypes: {
    onClickClearCompleted: { action: "onClickClearCompleted" },
    onClickListTypeButton: { action: "onClickListTypeButton" },
    listType: {
      control: { type: "select" },
      options: ["all", "active", "completed"],
    },
  },
};

const Template = (args) => createFooter(args);

export const StatusSection = Template.bind({});
StatusSection.args = {
  listType: "all",
  todoList: [
    { id: 1, completed: false, label: "항목1" },
    { id: 2, completed: true, label: "항목2" },
    { id: 3, completed: true, label: "항목3" },
    { id: 4, completed: true, label: "항목4" },
    { id: 5, completed: false, label: "항목5" },
  ],
};
