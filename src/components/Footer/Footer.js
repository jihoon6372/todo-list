import { createLeftSection } from "./LeftSection";
import { CenterSection } from "./CenterSection";
import { createRightSection } from "./RightSection";
import "./footer.css";

export const createFooter = (props) => {
  const { todoList, listType, onClickClearCompleted, onClickListTypeButton } =
    props;

  const footer = document.createElement("div");
  footer.classList.add("footer-wrap");

  // 좌측 영역 컴포넌트 추가
  const activeTodoList = todoList.filter((todo) => !todo.completed);
  const leftSection = createLeftSection(activeTodoList.length);
  footer.appendChild(leftSection);

  // 중앙 영역 컴포넌트
  const centerSection = new CenterSection({
    listType,
    onClickButton: onClickListTypeButton,
  });
  footer.appendChild(centerSection.createCenterSection());

  // 우측 영역 컴포넌트 추가
  const completedTodoList = todoList.filter((todo) => todo.completed);
  const rightSection = createRightSection({
    count: completedTodoList.length,
    onClick: onClickClearCompleted,
  });
  footer.appendChild(rightSection);

  return footer;
};
