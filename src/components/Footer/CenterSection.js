import { createFilterButton } from "./FilterButton";

export class CenterSection {
  listType = null;
  onClickButton = () => {};

  constructor(props) {
    this.listType = props?.listType || "all";
    this.onClickButton = props?.onClickButton || function () {};
  }

  createCenterSection() {
    // wrap 엘리먼트 생성
    const centerSection = document.createElement("div");

    // 클래스 설정
    centerSection.classList.add("footer-center");

    // All 버튼 컴포넌트 추가
    const allBtn = createFilterButton({
      label: "All",
      isActive: this.listType === "all",
      onClick: () => this.onClickButton("all"),
    });
    centerSection.appendChild(allBtn);

    // Active 버튼 컴포넌트 추가
    const activeBtn = createFilterButton({
      label: "Active",
      isActive: this.listType === "active",
      onClick: () => this.onClickButton("active"),
    });
    centerSection.appendChild(activeBtn);

    // Completed 버튼 컴포넌트 추가
    const completedBtn = createFilterButton({
      label: "Completed",
      isActive: this.listType === "completed",
      onClick: () => this.onClickButton("completed"),
    });
    centerSection.appendChild(completedBtn);

    return centerSection;
  }
}
