export const createRightSection = (props) => {
  const { count, onClick } = props;

  // 엘리먼트 생성
  const rightSection = document.createElement("div");

  // 클래스 추가
  rightSection.classList.add("footer-right");

  // 버튼 엘리먼트 추가
  const btn = document.createElement("button");

  // 버튼 클래스 추가
  btn.classList.add("btn");

  // 텍스트 설정
  btn.innerText = `Clear completed (${count})`;

  rightSection.appendChild(btn);

  // 클릭 이벤트 추가
  rightSection.addEventListener("click", onClick);

  return rightSection;
};
