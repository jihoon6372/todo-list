export const createLeftSection = (count = 0) => {
  // 엘리먼트 생성
  const leftSection = document.createElement("div");

  // 클래스 설정
  leftSection.classList.add("footer-left");

  // 텍스트 설정
  leftSection.innerText = `${count} items left`;

  return leftSection;
};
