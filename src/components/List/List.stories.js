import { createList } from "./List";

export default {
  title: "List Section",
  argTypes: {
    todoList: { control: "object" },
  },
};

const Template = (args) => createList(args);

const baseTodoList = [
  {
    id: "ff4ffbc0-26de-4bff-bde1-cc9895a66a86",
    label: "신규 입사자 교육",
    order: 1,
  },
  {
    id: "802e8ee4-3828-4bef-9185-6a199d7774db",
    label: "자바스크립트 교육",
    order: 2,
  },
];

export const NoTodoItems = Template.bind({});
NoTodoItems.args = {
  todoList: [],
};

export const TodoListWithItems = Template.bind({});
TodoListWithItems.args = {
  todoList: baseTodoList.map((todoList) => ({ ...todoList, completed: false })),
};

export const TodoListWithCheckedItems = Template.bind({});
TodoListWithCheckedItems.args = {
  todoList: baseTodoList.map((todoList) => ({ ...todoList, completed: true })),
};
