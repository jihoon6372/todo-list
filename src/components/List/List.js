import { createListItem } from "./ListItem";

export const createList = (props) => {
  const { todoList = [], onChange } = props;

  // Todo 리스트가 없을 경우
  if (todoList.length === 0) {
    const emptySet = document.createElement("div");
    emptySet.classList.add("empty");
    emptySet.innerText = "There are no to-do items. Please write your to-dos.";
    return emptySet;
  }

  // Todo 리스트가 있을 경우
  const list = document.createElement("ul");

  // 완료 되지 않은 Todo 리스트
  const activeTodoList = todoList.filter((todo) => !todo.completed);
  activeTodoList.sort((a, b) => a.order - b.order);

  // 완료 된 않은 Todo 리스트
  const completedTodoList = todoList.filter((todo) => todo.completed);
  completedTodoList.sort((a, b) => a.order - b.order);

  // 완료 되지 않은 리스트와 완료 된 리스트 따로 정렬
  [...activeTodoList, ...completedTodoList].forEach((todo) => {
    const item = createListItem({
      item: todo,
      onChange,
    });
    list.appendChild(item);
  });

  return list;
};
