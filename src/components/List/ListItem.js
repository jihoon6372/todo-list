import "./listItem.css";

export const createListItem = (props) => {
  const { item, onChange } = props;
  const listItemEl = document.createElement("li");

  // 체크박스
  const checkboxEl = document.createElement("input");
  checkboxEl.type = "checkbox";
  listItemEl.appendChild(checkboxEl);

  checkboxEl.onchange = (_) => {
    onChange(item.id);
  };

  // 라벨 추가
  const label = document.createElement("span");
  label.innerText = item.label;
  label.className = "label";
  listItemEl.appendChild(label);

  // 할일 항목 완료 되었을 경우
  if (item.completed) {
    checkboxEl.checked = true;

    // 클래스명 추가
    label.classList.add("completed");
  }

  return listItemEl;
};
