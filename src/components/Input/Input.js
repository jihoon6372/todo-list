import "./input.css";

export const createInput = (props) => {
  const { onEnter } = props;
  const wrap = document.createElement("div");
  const inp = document.createElement("input");
  inp.className = "inp";
  inp.type = "text";
  inp.placeholder = "What needs to be done?";

  inp.addEventListener("keypress", (e) => {
    if (e.key !== "Enter") return;
    if (e.target.value.trim() === "") return;

    onEnter(e.target.value);
    e.target.value = "";
  });

  wrap.appendChild(inp);
  wrap.classList.add("inp-wrap");

  return wrap;
};
