import { createInput } from "./Input";

export default {
  title: "Input Section",
  argTypes: {
    onEnter: { action: "onEnter" },
  },
};

const Template = (args) => createInput(args);

export const InputSection = Template.bind({});
InputSection.args = {
  onEnter: (value) => {},
};
