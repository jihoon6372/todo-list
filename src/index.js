import TodoListApp from "./TodoListApp";

(function () {
  // render
  const el = document.getElementById("app");

  const todoListApp = new TodoListApp(el);
  todoListApp.render();
})();
