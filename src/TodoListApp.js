import { createInput } from "./components/Input";
import { createList } from "./components/List";
import { createFooter } from "./components/Footer";
import { uuidv4 } from "./utils";
import "./index.css";

export default class TodoListApp {
  appElement = null;
  todoListElement = document.createElement("div");
  footerElement = document.createElement("div");
  listType = "all";
  todoList = [];

  constructor(el) {
    this.appElement = el;

    // list 엘리먼트 클래스 추가
    this.todoListElement.classList.add("todo-list");

    // footer 엘리먼트 클래스 추가
    this.footerElement.classList.add("footer");
  }

  // Todo 리스트 가져오기 함수
  getTodoList() {
    return this.todoList;
  }

  // Todo 리스트 설정 함수
  setTodoList(todoList) {
    this.todoList = todoList;
  }

  // Todo 항목 추가 함수
  addTodo(label) {
    // 현재 Todo 리스트 가져옴
    const prevTodoList = this.getTodoList();

    // 데이터 추가
    const nextTodoList = [
      {
        id: uuidv4(),
        completed: false,
        label,
      },
      ...prevTodoList,
    ].map((todo, index) => ({ ...todo, order: index + 1 }));

    // Todo 리스트 설정
    this.setTodoList(nextTodoList);

    // 부분 랜더링
    this.renderTodoListAndFooter();
  }

  // Todo 리스트 완료여부 토글 함수
  toggleCompletedTodoId(id) {
    // 현재 Todo 리스트 가져옴
    const prevTodoList = this.getTodoList();

    // 변경된 아이템 추출
    const findTodoItem = prevTodoList.find((todo) => todo.id === id);

    // 완료여부 상태값 변경
    findTodoItem.completed = !findTodoItem.completed;

    // 나머지 Todo 리스트 필터
    const filteredTodoList = prevTodoList.filter((todo) => todo.id !== id);

    // 완료 여부 상태값에 따라서 데이터 위치 정렬
    const nextTodoList = (
      findTodoItem.completed
        ? [...filteredTodoList, findTodoItem]
        : [findTodoItem, ...filteredTodoList]
    ).map((todo, index) => ({ ...todo, order: index + 1 }));

    // Todo 리스트 설정
    this.setTodoList(nextTodoList);

    // 부분 랜더링
    this.renderTodoListAndFooter();
  }

  // Todo 리스트
  getFilterTodoList(type) {
    const todoList = this.getTodoList();

    switch (type) {
      // 활성화
      case "active":
        return todoList.filter((todo) => !todo.completed);

      // 완료
      case "completed":
        return todoList.filter((todo) => todo.completed);

      // 기본값 전체
      case "all":
      default:
        return todoList;
    }
  }

  // 완료항목 초기화 함수
  clearCompleted() {
    // 현재 Todo 리스트
    const prevTodoList = this.getTodoList();

    // 완료되지 않은 리스트 필터
    const nextTodoList = prevTodoList.filter((todo) => !todo.completed);

    // Todo 리스트 설정
    this.setTodoList(nextTodoList);

    // 초기화 후 부분 랜더링
    this.renderTodoListAndFooter();
  }

  // 필터 설정 함수
  setListType(type) {
    this.listType = type;

    // 초기화 후 부분 랜더링
    this.renderTodoListAndFooter();
  }

  // 필터 타입 가져오기 함수
  getListType() {
    return this.listType;
  }

  // Todo 리스트, footer 엘리먼트 초기화 함수
  clearTodoListAndFooter() {
    this.todoListElement.textContent = null;
    this.footerElement.textContent = null;
  }

  // 전체 엘리먼트 초기화
  initAllElement() {
    this.appElement.textContent = null;
    this.clearTodoListAndFooter();
  }

  // Todo 리스트와 footer 랜더링 함수
  renderTodoListAndFooter() {
    // Todo 리스트와 footer 데이터 초기화
    this.clearTodoListAndFooter();

    // Todo 리스트 엘리먼트 생성 후 추가
    const todoList = this.createTodoList();
    this.todoListElement.appendChild(todoList);

    // footer 엘리먼트 생성 후 추가
    const footer = this.createFooter();
    this.footerElement.appendChild(footer);
  }

  // Todo 리스트 엘리먼트 생성 함수
  createTodoList() {
    const listType = this.getListType();
    const todoList = this.getFilterTodoList(listType);

    // Todo 리스트 생성
    const todoListElement = createList({
      todoList,
      onChange: this.toggleCompletedTodoId.bind(this),
    });

    return todoListElement;
  }

  // Footer 엘리먼트 생성 함수
  createFooter() {
    const listType = this.getListType();
    const todoList = this.getTodoList();
    const footer = createFooter({
      todoList,
      listType,
      onClickClearCompleted: this.clearCompleted.bind(this),
      onClickListTypeButton: this.setListType.bind(this),
    });

    return footer;
  }

  render() {
    this.initAllElement();

    const wrapper = document.createElement("div");
    wrapper.className = "wrapper";

    // 입력 영역
    const input = createInput({
      onEnter: this.addTodo.bind(this),
    });

    // 투두 리스트 영역
    const todoList = this.createTodoList();
    this.todoListElement.appendChild(todoList);

    // 하단 영역
    const footer = this.createFooter();
    this.footerElement.appendChild(footer);

    wrapper.appendChild(input);
    wrapper.appendChild(this.todoListElement);
    wrapper.appendChild(this.footerElement);
    this.appElement.appendChild(wrapper);

    return this.appElement;
  }
}
