import createTodoListApp from "./TodoListApp";

// More on default export: https://storybook.js.org/docs/html/writing-stories/introduction#default-export
export default {
  title: "Todo List App",
  // More on argTypes: https://storybook.js.org/docs/html/api/argtypes
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/html/writing-stories/introduction#using-args
const el = document.createElement("div");
el.classList.add("app");
const todoAppList = new createTodoListApp(el);
const Template = () => {
  return todoAppList.render();
};

export const TodoListApp = Template.bind({});
// More on args: https://storybook.js.org/docs/html/writing-stories/args
TodoListApp.args = {};
